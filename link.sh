#!/bin/bash

files=(
  ".gitconfig"
  ".gitignore_global"
  ".vimrc"
  ".zshrc"
)

for i in "${files[@]}"
do
  ln -s $PWD/$i $HOME/$i
done
