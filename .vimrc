syntax on
set number
set autoindent
set smartindent
set tabstop=2
set shiftwidth=2
set expandtab
set colorcolumn=80
set backspace=indent,eol,start

autocmd BufNewFile,BufRead *.go setlocal noexpandtab tabstop=4 shiftwidth=4
autocmd BufNewFile,BufRead .gitconfig setlocal noexpandtab tabstop=4 shiftwidth=4
autocmd FileType make setlocal noexpandtab tabstop=4 shiftwidth=4
